#!/usr/bin/with-contenv bashio
# shellcheck shell=bash

declare -a options
bashio::log.info "Starting Minio..."

if [[ ! -d /data/minio ]] ; then
    mkdir -p /data/minio
fi

for var in $(bashio::config 'env_vars|keys'); do
    name=$(bashio::config "env_vars[${var}].name")
    value=$(bashio::config "env_vars[${var}].value")
    if [[ "${name}" =~ SECRET ]]; then
        bashio::log.info "Setting ${name}"
    elif [[ "${name}" =~ PASSW ]]; then
        bashio::log.info "Setting ${name}"
    else
        bashio::log.info "Setting ${name} to ${value}"
    fi
    export "${name}=${value}"
done

bashio::config.require.ssl
if bashio::config.true 'ssl'; then
    certfile=$(bashio::config 'certfile')
    keyfile=$(bashio::config 'keyfile')
    if [[ ! -d /data/certs ]] ; then
        mkdir -p /data/certs
    fi
    if [[ ! -f /data/certs/private.key ]] ; then
        ln -s "/ssl/${keyfile}" /data/certs/private.key 
    fi
    if [[ ! -f /data/certs/public.crt ]] ; then
        ln -s "/ssl/${certfile}" /data/certs/public.crt
    fi
    options+=(--certs-dir="/data/certs")
fi

options+=(/data/minio)

exec /usr/local/bin/minio server "${options[@]}" 
