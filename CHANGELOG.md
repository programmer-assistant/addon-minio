# Changelog

# [0.3.0](https://gitlab.com/programmer-assistant/addon-minio/compare/v0.2.0...v0.3.0) (2021-10-21)


### :bug:

* Update location of minio data ([de85d2a](https://gitlab.com/programmer-assistant/addon-minio/commit/de85d2a2cc6b16dc3aec0e5325d145d974ab4255))

# [0.2.0](https://gitlab.com/programmer-assistant/addon-minio/compare/v0.1.6...v0.2.0) (2021-09-19)


### :arrow_up:

* Update image path and upgrade base image ([0737e2b](https://gitlab.com/programmer-assistant/addon-minio/commit/0737e2bf58029903c8404b7280cf3ebfd0fca084))

## 0.1.5

- Update minio to RELEASE.2021-09-15T04-54-25Z

## 0.1.4

- Replace ingress with webui

## 0.1.3

- Update minio entry

## 0.1.2

- Hide passwords from logs

## 0.1.1

- Add ingress

## 0.1.0

- Initial build based on minio RELEASE.2021-04-06T23-11-00Z
